# Touch Simulator

<!-- ------------------------ -->

## About

The Touch Simulator ([that can be found here](https://gitlab.com/publiclastgameboard/touch-simulator)) allows you to simulate the touch messages that are sent by the Gameboard without having to build and run your application on a Gameboard. All of this information can be found in our docuentation [here](https://external-codelabs.hosting.lastgameboard.com/codelabs/touch_simulator-codelab/index.html).

![Step-by-Step](assets/simulator.png)

<!-- ------------------------ -->

## Running the Simulator

### **Start the Simulator**

When you start up the simulator, this will be the first screen you see.

**You can leave all of the default values**, the SDK is looking to connect to the ports with those default values. Click the start button to begin.

The **transparent option** is currently only available on Windows. (See Transparent Window Section)

![Step-by-Step](assets/windowsStartScreen.PNG)

### **Start your game**

After the simulator is running, you can start your game that is connected to the Gameboard SDK. When your game starts and starts up the gameboard SDK, it will now be able to connect to the touch simulator that is running.

### **Simulate touches**

You can now drag tokens and hand blades onto the middle of the screen that is simulating a gameboard screen. The touch events should now be picked up in your game.

![Step-by-Step](assets/simulator.png)

Each token has a `tokenId` and a `sessionId`. The `tokenId` is always the same, indicating what shape is on the bottom of the token. The `sessionId` is updated with each movement.

![Step-by-Step](assets/token.png)

## Adding Tokens

You can add Handblades or Tokens with the buttons on the screen. When selecting to add a Token, you have a few options. Select from the dropdown before clicking **Add Token** to customize the generated token:

- Random Shape
  - Random shape will be selected from the list of possible shapes
- Custom Shape ID
  - An input will appear where you can enter a custom shapeID. If you enter an existing shape ID, that shape will be used.
- Select from the list of shapes available

  ![Step-by-Step](assets/shapeSelector.PNG)

  ![Step-by-Step](assets/customId.PNG)

## Interacting with Tokens

You can do the following actions:

- Drag tokens / hand blades

- Click and drag mouse to simulate finger/pointer touches
- Double tap a token to keep it pressed down while not touching
  - Tokens will be more opaque when they are considered 'placed' on the simulated board. They will be more transparent when 'lifted' off the simulated board.
- While a token is on the surface, **hold shift** before dragging to emulating picking up the piece instead of dragging along the surface
- Use keys **Q** and **E** to rotate objects
- Remove any tokens by right clicking

  **To see an example of how to receive touch events, see our [documentation on the TouchController.](https://external-codelabs.hosting.lastgameboard.com/codelabs/sdk_touch_controller-codelab/)**

  **NOTE:** _If you change the settings in the simulator at any time, it will restart the server, so you will also need to restart your game to reconnect properly._

## Adjust Touch Space

If you want to adjust or move the simulator space to better align with the game underneath, you can do that with the following controls.

### Size

Click the **+** or **-** icons to grow or shrink the simulator screen size. The screen will not grow larger than the application.

### Location

You can drag the location of the simulator screen by holding CTRL and dragging the simulator space.
![Step-by-Step](assets/simulatorAdjust.PNG)

<!-- ------------------------ -->

## (Windows Support Only) Transparent Window

By selecting the **Transparent** option, the Touch Simulator will be see through, so you have the option to put your game behind the simulator adjusted to the simulated touch surface to help with debugging touch interaction in your game.

The simulated surface will be slightly visible, so you can line up your game properly.

![Step-by-Step](assets/transparent.png)

<!-- ------------------------ -->
